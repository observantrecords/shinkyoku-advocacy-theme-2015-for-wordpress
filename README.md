# Shinkyoku Advocacy 2015 Theme

A custom theme for [Shinkyoku Advocacy](http://shinkyokuadvocacy.com/).

## Dependencies

* [Observant Records 2015 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2015-for-wordpress)
